const express = require('express');
const ctrl = require('../../controllers/subscriptions');
const { validateBody } = require('../../middlewares');
const { schemas } = require('../../models/subscription');

const router = express.Router();


router.post('/', validateBody(schemas.addSubscriptionSchema), ctrl.addSubscription);


module.exports = router;
