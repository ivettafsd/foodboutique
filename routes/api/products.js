const express = require('express');
const ctrl = require('../../controllers/products');
const {isValidId } = require('../../middlewares');

const router = express.Router();

router.get('/', ctrl.getAllProducts);
router.get('/popular', ctrl.getPopularProducts);
router.get('/discount', ctrl.getDiscountProducts);
router.get('/categories', ctrl.getAllCategories)
router.get('/:id', isValidId, ctrl.getProductById);


module.exports = router;
