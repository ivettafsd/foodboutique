const express = require('express');
const ctrl = require('../../controllers/orders');
const { validateBody } = require('../../middlewares');
const { schemas } = require('../../models/order');

const router = express.Router();

router.post('/',
    validateBody(schemas.createOrderSchema),
    ctrl.createOrder);


module.exports = router;
