const { handleMangooseError } = require('../helpers');
const { Schema, model } = require('mongoose');
const Joi = require('joi');

const emailRegexp = /^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/;

const orderSchema = new Schema(
  {
    email: {
      type: String,
      match: emailRegexp,
      required: true,
    },
    products: {
      type: [
        {
          productId: {
            type: String,
            ref: 'product',
          },
          amount: Number,
          _id: false,
        },
      ],
      default: [],
      required: true,
    },
  },
  { versionKey: false, timestamps: true },
);

orderSchema.post('save', handleMangooseError);

const createOrderSchema = Joi.object({
  email: Joi.string().pattern(emailRegexp).required().messages({
    'string.base': 'The email must be a string.',
    'string.pattern.base': 'The email must be in format test@gmail.com.',
    'any.required': 'The email field is required.',
  }),
  products: Joi.array()
    .items(
      Joi.object({
        productId: Joi.string().required(),
        amount: Joi.number().min(1).required(),
      }).required(),
    )
    .min(1)
    .required(),
});

const Order = model('order', orderSchema);
const schemas = { createOrderSchema };

module.exports = { Order, schemas };
