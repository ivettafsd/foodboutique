const { handleMangooseError } = require('../helpers');
const { Schema, model } = require('mongoose');
const { any } = require('../middlewares/upload');

const countertSchema = new Schema(
  {
    name: {
      type: String,
      unique: true,
      required: true,
    },
    value: {
      type: Date,
      required: true,
    },
  },
  { versionKey: false, timestamps: true },
);

countertSchema.post('save', handleMangooseError);

const Counter = model('counter', countertSchema);

module.exports = { Counter };
