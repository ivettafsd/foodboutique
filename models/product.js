const { handleMangooseError } = require('../helpers');
const { Schema, model } = require('mongoose');

const productSchema = new Schema(
  {
    _id: {
      type: Schema.Types.ObjectId,
      required: true,
    },
    category: {
      type: String,
      enum: [
        'Meat_&_Seafood',
        'Pantry_Items',
        'Fresh_Produce',
        'Beverages',
        'Breads_&_Bakery',
        'Dairy',
        'Deli',
        'Prepared_Foods',
        'Eggs',
        'Frozen_Foods',
        'Snacks',
      ],
      required: true,
    },
    name: {
      type: String,
      required: true,
    },
    desc: {
      type: String,
      required: true,
    },
    img: {
      type: String,
      required: true,
      default: 'https://res.cloudinary.com/dlraat4ys/image/upload/v1694967674/Mask_group_1_srzpv8.png',
    },
    price: {
      type: Number,
      required: true,
    },
    size: {
      type: String,
      required: true,
    },
    is10PercentOff: {
      type: Boolean,
      required: true,
    },
    popularity: {
      type: Number,
      required: true,
      default: 0,
    },
  },
  { versionKey: false, timestamps: true },
);

productSchema.post('save', handleMangooseError);

const Product = model('product', productSchema);

module.exports = { Product };
