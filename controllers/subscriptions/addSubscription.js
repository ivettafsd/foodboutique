const { Subscription } = require('../../models/subscription');
const { HttpError } = require('../../helpers');

const addSubscription = async (req, res) => {
  const { email } = req.body;

  const foundSubscription = await Subscription.findOne({ email });

  if (foundSubscription) {
    throw HttpError(409, 'Subscription already exists');
  }

  await Subscription.create({ email });

  const message = `Welcome to the Food Boutique! 🥦🍓 With Food Boutique, you're not just subscribing to food, you're signing up for a fresher, fitter, and happier you. Get ready to elevate your wellness journey, one bite at a time!`;
  res.status(201).json({ message });
};

module.exports = addSubscription;
