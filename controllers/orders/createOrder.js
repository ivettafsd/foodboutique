const { Order } = require('../../models/order');

const createOrder = async (req, res) => {
  const order = req.body;

  await Order.create(order);

  const message = `Order Success! Thank you for shopping at Food Boutique. Your order has been received and is now being freshly prepared just for you! Get ready to indulge in nourishing goodness, delivered right to your doorstep. We're thrilled to be part of your journey to better health and happiness. 🌿🍎`;
  res.status(201).json({ message });
};

module.exports = createOrder;
