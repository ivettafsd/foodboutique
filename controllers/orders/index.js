const { CtrlWrapper } = require('../../helpers');
const createOrder = require('./createOrder');

module.exports = {
  createOrder: CtrlWrapper(createOrder),
};
