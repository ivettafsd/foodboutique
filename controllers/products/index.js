const { CtrlWrapper } = require('../../helpers');
const getAllProducts = require('./getAllProducts');
const getProductById = require('./getProductById');
const getPopularProducts = require('./getPopularProducts');
const getDiscountProducts = require('./getDiscountProducts');
const getAllCategories = require('./getAllCategories');

module.exports = {
  getAllProducts: CtrlWrapper(getAllProducts),
  getProductById: CtrlWrapper(getProductById),
  getPopularProducts: CtrlWrapper(getPopularProducts),
  getDiscountProducts: CtrlWrapper(getDiscountProducts),
  getAllCategories: CtrlWrapper(getAllCategories)
};
