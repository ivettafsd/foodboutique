const { Product } = require('../../models/product');

const getPopularProducts = async (req, res) => {
  const { limit = 5 } = req.query;

  const popularProduct = await Product.find().sort({ popularity: -1 }).limit(limit).select('-updatedAt -desc');

  res.json(popularProduct);
};

module.exports = getPopularProducts;
