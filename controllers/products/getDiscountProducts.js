const { Product } = require('../../models/product');

const getDiscountProducts = async (req, res) => {
  const discountProducts = await Product.find({ is10PercentOff: true });

  res.json(discountProducts);
};

module.exports = getDiscountProducts;
