const { Product } = require('../../models/product');

const getAllCategories = async (req, res) => {
  const categories = await Product.distinct('category');
  res.json(categories);
};

module.exports = getAllCategories;
