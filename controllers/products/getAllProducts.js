const { Product } = require('../../models/product');
const { Counter } = require('../../models/counter');
const { ObjectId } = require('mongodb');

const getAllProducts = async (req, res) => {
  const checkActualDiscounts = await Counter.findOne({ name: 'Discount' });
  const currentDate = new Date();

  if (
    !checkActualDiscounts ||
    !(
      checkActualDiscounts.value.getFullYear() === currentDate.getFullYear() &&
      checkActualDiscounts.value.getMonth() === currentDate.getMonth() &&
      checkActualDiscounts.value.getDate() === currentDate.getDate()
    )
  ) {
    if (!checkActualDiscounts) {
      await Counter.create({ name: 'Discount', value: currentDate });
    } else {
      await Counter.findByIdAndUpdate(checkActualDiscounts._id, { value: currentDate });
      await Product.updateMany(
        {},
        {
          $set: {
            is10PercentOff: false,
          },
        },
      );
    }
    const totalDocuments = await Product.countDocuments();
    const randomOffset = Math.floor(Math.random() * (totalDocuments - 10));
    const randomDocuments = await Product.find({}).skip(randomOffset).limit(10);

    for (const doc of randomDocuments) {
      const filter = { _id: ObjectId(doc._id) };
      const updateDocument = {
        $set: {
          is10PercentOff: true,
        },
      };
      await Product.updateOne(filter, updateDocument);
    }
  }

  const { keyword = '', category = '', byABC = true, byPrice, byPopularity, page = 1, limit = 6 } = req.query;

  const query = {};
  keyword && (query.name = { $regex: keyword, $options: 'i' });
  category && (query.category = category);

  let sortOptions = { name: JSON.parse(byABC) ? 1 : -1 };
  if (byPrice !== undefined) {
    sortOptions = { price: JSON.parse(byPrice) ? 1 : -1 };
  }
  if (byPopularity !== undefined) {
    sortOptions = { popularity: JSON.parse(byPopularity) ? 1 : -1 };
  }

  const skip = (page - 1) * limit;

  const productsByQuery = await Product.aggregate([
    { $match: query },
    {
      $facet: {
        totalCount: [{ $count: 'count' }],
        results: [
          { ...(sortOptions && { $sort: sortOptions }) },
          { $skip: Number(skip) },
          { $limit: Number(limit) },
          { $unset: ['createdAt', 'updatedAt', 'desc'] },
        ],
      },
    },
    {
      $project: {
        totalCount: { $arrayElemAt: ['$totalCount.count', 0] },
        page: page,
        perPage: limit,
        results: 1,
      },
    },
  ]);
  res.json({
    page: Number(page),
    perPage: Number(limit),
    totalPages: Math.ceil(productsByQuery[0].totalCount / limit) || 0,
    results: productsByQuery[0].results,
  });
};

module.exports = getAllProducts;
