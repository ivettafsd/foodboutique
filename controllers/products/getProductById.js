const { Product } = require('../../models/product');
const { Types } = require('mongoose');
const { HttpError } = require('../../helpers');
const ObjectId = Types.ObjectId;

const getProductById = async (req, res) => {
    const { id } = req.params;

  const updatedProduct = await Product.findByIdAndUpdate(
    ObjectId.createFromHexString(id),
    {
      $inc: { popularity: 1 },
    },
    { new: true, select: '-createdAt -updatedAt' },
  );
  if (!updatedProduct) {
    throw HttpError(404, 'Not found');
  }

  res.json(updatedProduct);
};

module.exports = getProductById;
